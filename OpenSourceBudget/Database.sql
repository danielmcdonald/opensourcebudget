{\rtf1\ansi\ansicpg1252\cocoartf1404\cocoasubrtf340
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
\paperw11900\paperh16840\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\fs24 \cf0 -- phpMyAdmin SQL Dump\
-- version 4.4.10\
-- http://www.phpmyadmin.net\
--\
-- Host: localhost\
-- Generation Time: Mar 29, 2016 at 01:03 PM\
-- Server version: 5.5.42\
-- PHP Version: 7.0.0\
\
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";\
SET time_zone = "+00:00";\
\
--\
-- Database: `OpenSourceBudgetDatabase`\
--\
\
-- --------------------------------------------------------\
\
--\
-- Table structure for table `Transaction Table`\
--\
\
CREATE TABLE `Transaction Table` (\
  `Deposit` float NOT NULL,\
  `Withdrawal` float NOT NULL,\
  `RunTotal` float NOT NULL,\
  `Date` date NOT NULL,\
  `TransID` bigint(20) NOT NULL,\
  `IDNum` int(11) NOT NULL\
) ENGINE=InnoDB DEFAULT CHARSET=latin1;\
\
-- --------------------------------------------------------\
\
--\
-- Table structure for table `UserTable`\
--\
\
CREATE TABLE `UserTable` (\
  `FName` varchar(30) NOT NULL,\
  `LName` varchar(30) NOT NULL,\
  `EAddress` varchar(100) NOT NULL,\
  `PNum` int(11) NOT NULL,\
  `PassW` varchar(32) NOT NULL,\
  `IDNum` int(11) NOT NULL\
) ENGINE=InnoDB DEFAULT CHARSET=latin1;\
\
--\
-- Indexes for dumped tables\
--\
\
--\
-- Indexes for table `Transaction Table`\
--\
ALTER TABLE `Transaction Table`\
  ADD PRIMARY KEY (`TransID`);\
\
--\
-- Indexes for table `UserTable`\
--\
ALTER TABLE `UserTable`\
  ADD PRIMARY KEY (`IDNum`);\
\
--\
-- AUTO_INCREMENT for dumped tables\
--\
\
--\
-- AUTO_INCREMENT for table `Transaction Table`\
--\
ALTER TABLE `Transaction Table`\
  MODIFY `TransID` bigint(20) NOT NULL AUTO_INCREMENT;\
--\
-- AUTO_INCREMENT for table `UserTable`\
--\
ALTER TABLE `UserTable`\
  MODIFY `IDNum` int(11) NOT NULL AUTO_INCREMENT;}