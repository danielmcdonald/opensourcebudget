        <div class="clear"></div>
    </main>
    <footer>
        <p>&copy; Open Source budget</p>
    </footer>
</body>
</html>
<%
    // Set the "last visited page" session unless told not to
    if(request.getAttribute("setlastpage") == null) {
        request.getSession(true).setAttribute("lastpage", request.getRequestURI()+ "?" + request.getQueryString());
    }
%>