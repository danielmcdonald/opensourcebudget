<%@page import="java.util.ArrayList"%>
<%    
    // Initialise the last visited page to this page
    if(request.getSession(true).getAttribute("lastpage") == null) {
        request.getSession().setAttribute("lastpage", request.getRequestURI() + "?" + request.getQueryString());
    }
    
    // Page title (append to this)
    request.setAttribute("OpenSourceBudget.template.title", "OpenSourceBudget");
    
    // Array of CSS files to include
    ArrayList<String> cssFiles = new ArrayList();
    cssFiles.add("css/common.css");
    request.setAttribute("fedunibooks.template.css", cssFiles);
    
    // Array of JavaScript files to include
    /*ArrayList<String> jsFiles = new ArrayList();
    jsFiles.add("js/common.js");
    request.setAttribute("JavaScript.template.js", jsFiles);*/
%>