<%-- 
    Document   : template_head
    Created on : Mar 22, 2016, 7:03:32 AM
    Author     : Ben
--%>

<%@page import="java.util.ArrayList"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
           <link rel="stylesheet" type="text/css" href="css/common.css">
            <!--The Following Script is form google charts https://google-developers.appspot.com/chart/interactive/docs/gallery/piechart#example
                And therefor released under it's own orginal licences and not the licence associated with this project-->
            <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
            <script type="text/javascript">
              google.charts.load('current', {'packages':['corechart']});
              google.charts.setOnLoadCallback(drawChart);
              function drawChart() {

                var data = google.visualization.arrayToDataTable([
                  ['Task', 'Hours per Day'],
                  ['Rent',     11],
                  ['Food',      2],
                  ['Bills',    7]
                ]);

                var options = {
                  title: 'Expenses'
                };

                var chart = new google.visualization.PieChart(document.getElementById('piechart'));

                chart.draw(data, options);
              }
            </script>
    </head>
    <body>
        <ul>
            <li><a href=""> Budget</a></li>
            <li><a href=""> Cash Flow Chart</a></li>
            <li><a href=""> User Account</a></li>
            <li><a href=""> Re-occurring Transactions?</a></li>    
            <li><a href=""> login/logout</a></li>  
        </ul>
            
            <!--place a standard page header in here -->
    <main>
